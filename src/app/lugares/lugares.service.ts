import { Injectable } from '@angular/core';
import { Lugar } from './lugar.model';
import { FnParam } from '@angular/compiler/src/output/output_ast';
import { LoginService } from './../login/login.service';
import { BehaviorSubject, of } from 'rxjs';
import { take, map, delay, tap, switchMap } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';


interface LugarData{
  descripcion: string;
  disponibleDesde: string;
  disponibleHasta: string;
  id: number;
  imageUrl: string;
  precio: number;
  titulo: string;
  usuarioId: number;
}
@Injectable({
  providedIn: 'root'
})
export class LugaresService {

  // tslint:disable-next-line:variable-name
  private _lugares = new BehaviorSubject<Lugar[]>([

  // tslint:disable-next-line:max-line-length
  /*  new Lugar(1, 'Quinta Gonzalez', 'Quinta con excelente ubicacion', 'https://img10.naventcdn.com/avisos/18/00/52/30/54/90/1200x1200/63671397.jpg', 1200, new Date('2020-10-01'), new Date('2021-01-01'), 1), */

  // tslint:disable-next-line:max-line-length
  /*  new Lugar(2, 'Depto. Las Torres', 'Apartamento con excelente ubicacion', 'https://img10.naventcdn.com/avisos/18/00/53/55/97/00/720x532/144271181.jpg', 2400, new Date('2020-10-01'), new Date('2021-01-01'), 1), */
  // tslint:disable-next-line:max-line-length
  //  new Lugar(3, 'Cumbres Elite', 'Apartamento con excelente ubicacion', 'https://cf.bstatic.com/images/hotel/max1024x768/174/174836075.jpg', 1800, new Date('2020-10-01'), new Date('2021-01-01'), 1),

  // tslint:disable-next-line:max-line-length
  //  new Lugar(11, 'Quinta Gonzalez', 'Quinta con excelente ubicacion', 'https://img10.naventcdn.com/avisos/18/00/52/30/54/90/1200x1200/63671397.jpg', 1200, new Date('2020-10-01'), new Date('2021-01-01'), 1),

  // tslint:disable-next-line:max-line-length
  //  new Lugar(12, 'Depto. Las Torres', 'Apartamento con excelente ubicacion', 'https://img10.naventcdn.com/avisos/18/00/53/55/97/00/720x532/144271181.jpg', 2400, new Date('2020-10-01'), new Date('2021-01-01'), 1),
  // tslint:disable-next-line:max-line-length
  //  new Lugar(13, 'Cumbres Elite', 'Apartamento con excelente ubicacion', 'https://cf.bstatic.com/images/hotel/max1024x768/174/174836075.jpg', 1800, new Date('2020-10-01'), new Date('2021-01-01'), 1),
  // tslint:disable-next-line:max-line-length
  //  new Lugar(21, 'Quinta Gonzalez', 'Quinta con excelente ubicacion', 'https://img10.naventcdn.com/avisos/18/00/52/30/54/90/1200x1200/63671397.jpg', 1200, new Date('2020-10-01'), new Date('2021-01-01'), 1),
  // tslint:disable-next-line:max-line-length
  //  new Lugar(22, 'Depto. Las Torres', 'Apartamento con excelente ubicacion', 'https://img10.naventcdn.com/avisos/18/00/53/55/97/00/720x532/144271181.jpg', 2400, new Date('2020-10-01'), new Date('2021-01-01'), 1),
  // tslint:disable-next-line:max-line-length
  //  new Lugar(23, 'Cumbres Elite', 'Apartamento con excelente ubicacion', 'https://cf.bstatic.com/images/hotel/max1024x768/174/174836075.jpg', 1800, new Date('2020-10-01'), new Date('2021-01-01'), 1),
  // tslint:disable-next-line:max-line-length
  //  new Lugar(31, 'Quinta Gonzalez', 'Quinta con excelente ubicacion', 'https://img10.naventcdn.com/avisos/18/00/52/30/54/90/1200x1200/63671397.jpg', 1200, new Date('2020-10-01'), new Date('2021-01-01'), 1),
  // tslint:disable-next-line:max-line-length
  //  new Lugar(32, 'Depto. Las Torres', 'Apartamento con excelente ubicacion', 'https://img10.naventcdn.com/avisos/18/00/53/55/97/00/720x532/144271181.jpg', 2400, new Date('2020-10-01'), new Date('2021-01-01'), 1),
  // tslint:disable-next-line:max-line-length
  //  new Lugar(33, 'Cumbres Elite', 'Apartamento con excelente ubicacion', 'https://cf.bstatic.com/images/hotel/max1024x768/174/174836075.jpg', 1800, new Date('2020-10-01'), new Date('2021-01-01'), 1)
  ]);

  get lugares(){
    return this._lugares.asObservable();
  }

  constructor(private loginService: LoginService, private http: HttpClient ) { }
  fetchLugares(){
    return this.http.get<{[key: string]: LugarData}>('https://fake-bnb-10.firebaseio.com/ofertas-lugares.json')
    .pipe(map( dta => {
      const lugares = [];
      for (const key in dta){
        if (dta.hasOwnProperty(key)){
          lugares.push(
            // dta
            new Lugar(dta[key].id, dta[key].titulo,
              dta[key].descripcion, dta[key].imageUrl, dta[key].precio,
              new Date(dta[key].disponibleDesde), new Date(dta[key].disponibleHasta),
              dta[key].usuarioId, key)
          // tslint:disable-next-line:semicolon
          )
        }
      }
      return lugares;
    }),
    tap(lugares => {
      // tslint:disable-next-line:semicolon
      this._lugares.next(lugares)
    })
    );
  }
  getLugar(firebaseId: string){
    return this.http
    .get<LugarData>('https://fake-bnb-10.firebaseio.com/ofertas-lugares/${firebaseId}.json')
    .pipe(
      map(dta => {
        return new Lugar(dta.id, dta.titulo, dta.descripcion,
          dta.imageUrl, dta.precio, new Date(dta.disponibleDesde),
          new Date(dta.disponibleHasta), dta.usuarioId, firebaseId);
      })
    );
  }

  addLugar(titulo: string, descripcion: string, precio: number, disponibleDesde: Date, disponibleHasta: Date){
    const newLugar = new Lugar(
      Math.random(),
      titulo,
      descripcion,
      'https://img10.naventcdn.com/avisos/18/00/53/55/97/00/720x532/144271181.jpg',
      precio,
      disponibleDesde,
      disponibleHasta,
      this.loginService.usuarioId,
      ''
    );
    this.http.post <any>('https://fake-bnb-10.firebaseio.com/ofertas-lugares.json', { ...newLugar, firebaseId: null}).subscribe(data => {
    console.log(data);
    return this._lugares.pipe(take(1)).subscribe(lugares => {
      this._lugares.next(lugares.concat(newLugar));
    });
    });
  }

  updateLugar(lugarId: number, titulo: string, descripcion: string)
  {
    let nuevosLugares: Lugar[];
    return this.lugares.pipe(take(1),
    switchMap(lugares => {

      if (!lugares || lugares.length <= 0){
        return this.fetchLugares();
      }
      else{
        return of(lugares);
      }
    }),
    switchMap(lugares => {
      const index = lugares.findIndex(lu => lu.firebaseId === lugarId);
      nuevosLugares = [...lugares];
      const old = nuevosLugares[index];
      nuevosLugares[index] = new Lugar(old.id, titulo,
          descripcion, old.imageUrl, old.precio, old.disponibleDesde,
          old.disponibleHasta, old.usuarioId, '');

      return this.http.put('https://fake-bnb-10.firebaseio.com/ofertas-lugares/${lugarId}.json', {...nuevosLugares[index]});
    }), tap(() => {
      this._lugares.next(nuevosLugares);
    })
    );
  }

}
