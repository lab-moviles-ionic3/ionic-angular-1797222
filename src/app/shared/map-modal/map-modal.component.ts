import { Component, OnInit, AfterViewInit, ElementRef, Renderer2, ViewChild } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { map } from 'rxjs/operators';
import { environment } from './../../../environments/environment';

@Component({
  selector: 'app-map-modal',
  templateUrl: './map-modal.component.html',
  styleUrls: ['./map-modal.component.scss'],
})
export class MapModalComponent implements OnInit, AfterViewInit {

  @ViewChild('map') mapElement: ElementRef;
  constructor(private modalCtrl: ModalController, private render: Renderer2) { }

  ngOnInit() {}

  onCancel(){
    this.modalCtrl.dismiss();
  }

  ngAfterViewInit(){
    this.getGoogleMaps()
    .then(googleMaps => {
      const mapEl = this.mapElement.nativeElement;
      // tslint:disable-next-line:no-shadowed-variable
      const map = new googleMaps.Map( mapEl, {
        center: { lat: 25.7226326, lng: -100.3120671},
        zoom: 16
      });
      googleMaps.event.addListenerOnce(map, 'idle', () =>{
        this.renderer.addClass(mapEl, 'visible');
      });
      map.addListener('click', event => {
        const coords = {lat: event.latLng.lat(), lng:
        event.latLng.lng()};
        this.modalCtrl.dismiss(coords);
      });
    })
    .catch(err => {
      console.error(err);
    });
  }

  private getGoogleMaps(){
    const win = window as any;
    // tslint:disable-next-line:semicolon
    const googleModule = win.google

    if (googleModule && googleModule.maps){
      return Promise.resolve(googleModule.maps);
    }

    return new Promise((resolve, reject) => {
      const script = document.createElement('script');
    //  script.src = 'https://maps.googleapis.com/maps/api/js?key=AIzaSyAhzUtx8_C6MFnT9XqkAFPLPPCVNjSUoMc';
      script.src = 'https://maps.googleapis.com/maps/api/js?key=' + environment.googleMapsAPIKey;
      script.async = true;
      script.defer = true;
      document.body.appendChild(script);
      script.onload = () => {
        const loadedGoogleModule = win.google;

        if (loadedGoogleModule && loadedGoogleModule.maps){
          resolve(loadedGoogleModule.maps);
        }
        else{
          // tslint:disable-next-line:semicolon
          reject('Google Maps SDK no permitido :(')
        }
      // tslint:disable-next-line:semicolon
      }
    });
  }

}
